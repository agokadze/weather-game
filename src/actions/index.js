import { getIsFetching, getSelectedLocations } from "../reducers";
import * as api from "../api";

export const checkAnswer = id => (dispatch, getState) => {
  if (getIsFetching(getState())) {
    return Promise.resolve();
  }

  dispatch({
    type: "FETCH_WEATHERS_BY_LOCATIONS_REQUEST"
  });
  dispatch({
    type: "QUESTION_ANSWERED"
  });

  const locations = getSelectedLocations(getState());

  return api.fetchWeathersByLocations(locations).then(
    response => {
      dispatch({
        type: "FETCH_WEATHERS_BY_LOCATIONS_SUCCESS",
        id,
        locations: response
      });
    },
    error => {
      dispatch({
        type: "FETCH_WEATHERS_BY_LOCATIONS_FAILURE",
        message: error.message || "Something went wrong."
      });
    }
  );
};

export const selectNewLocations = () => ({
  type: "SELECT_NEW_LOCATIONS"
});

export const changeUnit = unit => ({
  type: "CHANGE_UNIT",
  unit
});
