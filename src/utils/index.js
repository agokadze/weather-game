export const getTempInUnit = (temp, unit) => {
  if (unit !== "celsius") temp = Math.floor((temp * 9) / 5.0 + 32);

  let result = temp + "";
  if (temp > 0) result = "+" + result;

  if (unit === "celsius") result += " C";
  else result += " F";
  return result;
};

export const getAnswerText = (win) => {
  if (win)
    return "CONGRATULATIONS: You WON!";
  else
    return "You LOST!";
};

