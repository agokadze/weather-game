import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import Score from "../Score";
import Location from "../Location";

import "./style.css";
import { getAnswerText } from "../../utils";

const Answer = ({ answer, onNextCitiesClick }) => (
  <div className={classnames("answer-container")}>
    <h1> {getAnswerText(answer.isCorrect)} </h1>
    <Score />
    <div className={classnames("locations")}>
      {answer.locations.map(location => (
        <Location
          key={location.id}
          {...location}
          {...answer.answeredLocationId === location.id && {
            bgColor: answer.isCorrect ? "#28a745" : "#dc3545"
          }}
        />
      ))}
    </div>
    <button onClick={onNextCitiesClick}>Next cities</button>
  </div>
);

Answer.propTypes = {
  answer: PropTypes.shape({
    locations: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
        temperature: PropTypes.number.isRequired
      }).isRequired
    ).isRequired,
    answeredLocationId: PropTypes.number.isRequired,
    isCorrect: PropTypes.bool.isRequired
  }),
  onNextCitiesClick: PropTypes.func.isRequired
};

export default Answer;
