import React from "react";
import { PropTypes } from "prop-types";
import classnames from "classnames";
import Location from "../Location";

import "./style.css";

const History = ({ history }) => (
  <div className={classnames("history-container")}>
    <h2>History</h2>
    <div className={classnames("items")}>
      {history.map((item, i) => (
        <div key={i} className={classnames("item")}>
          {item.locations.map(location => (
            <Location
              key={location.id}
              {...location}
              {...item.answeredLocationId === location.id && {
                bgColor: item.isCorrect ? "#28a745" : "#dc3545"
              }}
            />
          ))}
          <div className={classnames("item-answer")}>
            {item.isCorrect ? <span>&#10003;</span> : <span>&#10008;</span>}
          </div>
        </div>
      ))}
    </div>
  </div>
);

History.propTypes = {
  history: PropTypes.arrayOf(
    PropTypes.shape({
      locations: PropTypes.arrayOf(
        PropTypes.shape({
          id: PropTypes.number.isRequired,
          name: PropTypes.string.isRequired,
          temperature: PropTypes.number.isRequired
        })
      ),
      answeredLocationId: PropTypes.number.isRequired,
      isCorrect: PropTypes.bool.isRequired
    })
  )
};

export default History;
