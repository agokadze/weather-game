import React from "react";
import { connect } from "react-redux";
import { PropTypes } from "prop-types";
import classnames from "classnames";
import { getUnit } from "../../reducers";
import { getTempInUnit } from "../../utils";

import "./style.css";


let Location = ({ onClick, name, temperature, unit, bgColor }) => (
  <div
    className={classnames("location-container")}
    style={ {backgroundColor: bgColor}} 
    {...!!onClick && { onClick: onClick }}
  >
    <p>{name}</p>
    {!!temperature && <p>{getTempInUnit(temperature, unit)}</p>}
  </div>
);

Location.propTypes = {
  onClick: PropTypes.func,
  name: PropTypes.string.isRequired,
  temperature: PropTypes.number,
  bgColor: PropTypes.string,
};

const mapStateToProps = state => ({
  unit: getUnit(state)
});

Location = connect(mapStateToProps)(Location);

export default Location;
