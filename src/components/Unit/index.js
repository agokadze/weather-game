import React from "react";
import { PropTypes } from "prop-types";
import classnames from "classnames";

import "./style.css";

const Unit = ({ onChange, unit }) => (
  <div className={classnames("unit-container")}>
    <h2>Units</h2>
    <label htmlFor="celsius">
      <input
        type="radio"
        defaultChecked={"celsius" === unit}
        name="unit"
        value="celsius"
        id="celsius"
        onChange={() => onChange('celsius')}
      />
      Celsius
    </label>
    <label htmlFor="fahrenheit">
      <input
        type="radio"
        defaultChecked={"fahrenheit" === unit}
        name="unit"
        value="fahrenheit"
        id="fahrenheit"
        onChange={() => onChange('fahrenheit')}
      />
      Fahrenheit
    </label>
  </div>
);

Unit.propTypes = {
  onChange: PropTypes.func.isRequired,
  unit: PropTypes.string.isRequired
};

export default Unit;
