import React from 'react';
import { Provider } from 'react-redux'; 
import classnames from 'classnames'; 
import Home from '../containers/Home';
import Settings from '../containers/Settings';
import NotFound from './NotFound';
import Header from './Header';
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom';

import "./App.css";


const App = ({ store }) => (
  <Provider store={store}>
    <Router>
      <div className={classnames('container')}>
        <Header />
        <Switch>
          <Route exact={true} path='/' component={Home} />
          <Route exact={true} path='/settings' component={Settings} />
          <Route component={NotFound} />
        </Switch>
      </div>
    </Router>
  </Provider>
)

export default App;