import React from "react";
import Score from "../Score";
import Location from "../Location";
import classnames from "classnames";
import PropTypes from "prop-types";

import "./style.css";

const Question = ({ locations, onLocationClick }) => (
  <div className={classnames("question-container")}>
    <h1>Which city is hotter?</h1>
    <Score />
    <div className={classnames("locations")}>
      {locations.map(location => (
        <Location
          key={location.id}
          {...location}
          onClick={() => onLocationClick(location.id)}
        />
      ))}
    </div>
  </div>
);

Question.propTypes = {
  locations: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired
    }).isRequired
  ).isRequired,
  onLocationClick: PropTypes.func.isRequired
};

export default Question;
