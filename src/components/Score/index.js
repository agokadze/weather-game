import React from "react";
import { connect } from "react-redux";
import { getScore } from "../../reducers";
import "./style.css";

let Score = ({ score }) => (
  <div>
    Score <b>{score}</b>
  </div>
);

const mapStateToProps = state => ({
  score: getScore(state)
});

Score = connect(mapStateToProps)(Score);

export default Score;
