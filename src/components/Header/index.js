import React from 'react';
import RouterLink from '../../containers/RouterLink';

const Header = () => (
  <p>
    <RouterLink path="/settings">
      Settings
    </RouterLink>
    <RouterLink path="/">
      Back
    </RouterLink>
  </p>
);

export default Header;