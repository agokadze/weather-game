export const fetchWeathersByLocations = locations => {
  const ids = locations.map(location => location.id).join(",");

  const url = `http://api.openweathermap.org/data/2.5/group?id=${ids}&units=metric&appid=6983987cb55cfb8f5237038cfd17e0f1`;

  return fetch(url)
    .then(res => res.json())
    .then(data => {
      return data.list.map(item => ({
        name: item.name,
        temperature: item.main.temp,
        id: item.id
      }));
    });
};
