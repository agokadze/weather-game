import React from 'react';
import { NavLink } from 'react-router-dom';

const RouterLink = ({ path, children }) => (
  <NavLink
    exact
    to={path}
    style={{
      textDecoration: 'none',
      border: 'solid 1px black',
      padding: '6px',
      backgroundColor: '#D3D3D3',
      fontSize: '18px',
      color: 'black',
    }}
    activeStyle={{
      display: 'none',
    }}
  >
    {children}
  </NavLink>
)

export default RouterLink;