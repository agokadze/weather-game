import React, { Component } from 'react';
import { connect } from "react-redux";
import classnames from 'classnames';
import Unit from '../../components/Unit';
import History from '../../components/History';
import * as actions from "../../actions";
import { getUnit, getHistory } from '../../reducers';

import './style.css';


class Settings extends Component {
  render() {
    const {
      unit,
      changeUnit,
      history
    } = this.props;

    return (
      <div className={classnames('settings-container')}>
        <h1>
          Settings
        </h1>
        <Unit unit={unit} onChange={changeUnit} />
        <History history={history} />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  unit: getUnit(state),
  history: getHistory(state)
});

Settings = connect(
  mapStateToProps,
  actions
)(Settings);

export default Settings;