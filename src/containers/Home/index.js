import React, { Component } from "react";
import { connect } from "react-redux";
import Question from "../../components/Question";
import Answer from "../../components/Answer";
import FetchError from "../../components/FetchError";
import * as actions from "../../actions";
import {
  getErrorMessage,
  getIsFetching,
  getSelectedLocations,
  getWaitingForAnswer,
  getHistoryLastRecord
} from "../../reducers";


import "./style.css";

class Home extends Component {
  componentDidMount() {
    this.getNewQuestion();
  }

  getNewQuestion() {
    const { selectNewLocations } = this.props;
    selectNewLocations();
  }

  render() {
    const {
      waitingForAnswer,
      selectNewLocations,
      checkAnswer,
      locations,
      errorMessage,
      isFetching,
      answer
    } = this.props;

    if (isFetching && (!answer.locations || !answer.locations.length)) {
      return <p>Checking...</p>;
    }

    if (errorMessage && (!answer.locations || !answer.locations.length)) {
      return (
        <FetchError
          message={errorMessage}
          onRetry={() => this.getNewQuestion}
        />
      );
    }

    if (waitingForAnswer) {
      return <Question locations={locations} onLocationClick={checkAnswer} />;
    }

    return <Answer answer={answer} onNextCitiesClick={selectNewLocations} />;
  }
}

const mapStateToProps = state => ({
  locations: getSelectedLocations(state),
  answer: getHistoryLastRecord(state),
  errorMessage: getErrorMessage(state),
  isFetching: getIsFetching(state),
  waitingForAnswer: getWaitingForAnswer(state)
});

Home = connect(
  mapStateToProps,
  actions
)(Home);

export default Home;
