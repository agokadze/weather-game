import { combineReducers } from "redux";
import selectLocations, * as fromLocation from "./selectLocations";
import waitingForAnswer, * as fromWait from "./waitingForAnswer";
import score, * as fromScore from "./score";
import settings, * as fromSettings from "./settings";

const reducers = combineReducers({
  selectLocations,
  waitingForAnswer,
  score,
  settings
});

export default reducers;

export const getSelectedLocations = state =>
  fromLocation.getSelectedLocations(state.selectLocations);

export const getWaitingForAnswer = state =>
  fromWait.getWaitingForAnswers(state.waitingForAnswer);

export const getErrorMessage = state => fromScore.getErrorMessage(state.score);
export const getIsFetching = state => fromScore.getIsFetching(state.score);
export const getScore = state => fromScore.getScore(state.score);
export const getHistory = state => fromScore.getHistory(state.score);
export const getHistoryLastRecord = state => fromScore.getHistoryLastRecord(state.score);
export const getUnit = state => fromSettings.getUnit(state.settings);

