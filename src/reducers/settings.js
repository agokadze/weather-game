const settings = (state = { unit: 'celsius' }, action) => {
  switch (action.type) {
    case "CHANGE_UNIT":
      return {
        unit: action.unit
      };
    default:
      return state;
  }
};

export default settings;

export const getUnit = state => state.unit;
