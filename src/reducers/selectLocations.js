import cities from "../data/city.list.json";

const selectLocations = (state = {}, action) => {
  switch (action.type) {
    case "SELECT_NEW_LOCATIONS":
      let selectedLocations = [];
      for (let i = 0; i < 2; i++) {
        const city = cities[~~(Math.random() * cities.length)];
        selectedLocations.push({
          id: city.id + "",
          name: city.name
        });
      }
      return {
        ...state,
        selectedLocations
      };
    default:
      return state;
  }
};

export default selectLocations;

export const getSelectedLocations = state => state.selectedLocations || [];
