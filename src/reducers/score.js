import { combineReducers } from "redux";

const checkAnswer = (state = { score: 0, history: [] }, action) => {
  switch (action.type) {
    case "FETCH_WEATHERS_BY_LOCATIONS_SUCCESS":
      const maxTempLoc = action.locations.reduce((prev, current) => {
        return prev.temperature > current.temperature ? prev : current;
      });

      const isCorrect = maxTempLoc.id === +action.id;

      return {
        ...state,
        score: state.score + isCorrect,
        history: [
          {
            answeredLocationId: +action.id,
            locations: action.locations,
            isCorrect
          },
          ...state.history
        ]
      };
    default:
      return state;
  }
};

const isFetching = (state = false, action) => {
  switch (action.type) {
    case "FETCH_WEATHERS_BY_LOCATIONS_REQUEST":
      return true;
    case "FETCH_WEATHERS_BY_LOCATIONS_SUCCESS":
    case "FETCH_WEATHERS_BY_LOCATIONS_FAILURE":
      return false;
    default:
      return state;
  }
};

const errorMessage = (state = null, action) => {
  switch (action.type) {
    case "FETCH_WEATHERS_BY_LOCATIONS_FAILURE":
      return action.message;
    case "FETCH_WEATHERS_BY_LOCATIONS_REQUEST":
    case "FETCH_WEATHERS_BY_LOCATIONS_SUCCESS":
      return null;
    default:
      return state;
  }
};

const score = combineReducers({
  checkAnswer,
  errorMessage,
  isFetching
});

export default score;

export const getScore = state => state.checkAnswer.score;
export const getHistory = state => state.checkAnswer.history;
export const getHistoryLastRecord = state => {
  const history = state.checkAnswer.history;
  if (history && history.length) return history[0];
  return {};
};
export const getIsFetching = state => state.isFetching;
export const getErrorMessage = state => state.errorMessage;
