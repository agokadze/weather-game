const waitingForAnswer = (state = {waitingForAnswer: true}, action) => {
  switch (action.type) {
    case "SELECT_NEW_LOCATIONS":
      return {
        waitingForAnswer: true
      };
    case "QUESTION_ANSWERED":
      return {
        waitingForAnswer: false
      };
    default:
      return state;
  }
};

export default waitingForAnswer;

export const getWaitingForAnswers = state => state.waitingForAnswer;
